function draw(images) {
    var viewportWidth = window.innerWidth;
    var viewportHeight = window.innerHeight;

    var playgroundWidth;
    var playgroundHeight;
    var playgroundHalfHeight;
    var playgroundHeightWidthRatio = 2; // if height=200 then width=400, i.e. height-width-ratio=2
    var playgroundX;
    var playgroundY;
    var playgroundMargin = 10;

    var playerImage;
    var playerImageX;
    var playerImageY;
    var playerImageWidth;
    var playerImageHeight;
    var playerImageWidthHeightRatio;
    var playerImageScaleX;
    var playerImageScaleY;

    var opponentImage;
    var opponentImageX;
    var opponentImageY;
    var opponentImageWidth;
    var opponentImageHeight;
    var opponentImageWidthHeightRatio;
    var opponentImageScaleX;
    var opponentImageScaleY;

    var paddleImage;
    var paddleWidth;
    var paddleHeight;
    var paddleHalfWidth;
    var paddleWidthHeightRatio = 4; // if width=10 then height=40, i.e. width-height-ratio=4
    var paddleX;
    var paddleY;
    var paddleImageScaleX;
    var paddleImageScaleY;

    var ballImage;
    var ballRadius;
    var ballHeight;
    var ballWidth;
    var ballX;
    var ballY;
    var ballImageScaleX;
    var ballImageScaleY;

    var ballVelocityX;
    var ballVelocityY;
    var ballDeltaX;
    var ballDeltaY;

	var renderingElapsedTimeThreshold = 10; // Number of milliseconds before allowing a redraw
	var accumulatedTimeDiff = 0;

    if ((viewportWidth / viewportHeight) > playgroundHeightWidthRatio) {
        playgroundHalfHeight = Math.floor((viewportHeight - (2 * playgroundMargin)) / 2);
    } else {
        playgroundHalfHeight = Math.floor((viewportWidth - (2 * playgroundMargin)) / 4);
    }

    playgroundHeight = 2 * playgroundHalfHeight;
    playgroundWidth = playgroundHeight * playgroundHeightWidthRatio;
    playgroundX = Math.floor((viewportWidth - playgroundWidth) / 2);
    playgroundY = playgroundMargin;

    playerImage = images.dev;
    playerImageWidthHeightRatio = playerImage.width / playerImage.height;
    playerImageHeight = playgroundHalfHeight;
    playerImageWidth = Math.floor(playerImageHeight * playerImageWidthHeightRatio);
    playerImageX = playgroundX;
    playerImageY = playgroundY + Math.floor(playerImageHeight / 2);
    playerImageScaleX = playerImageWidth / playerImage.width;
    playerImageScaleY = playerImageHeight / playerImage.height;

    opponentImage = images.ops;
    opponentImageWidthHeightRatio = opponentImage.width / opponentImage.height;
    opponentImageHeight = playgroundHalfHeight;
    opponentImageWidth = Math.floor(opponentImageHeight * opponentImageWidthHeightRatio);
    opponentImageX = playgroundX + playgroundWidth - opponentImageWidth;
    opponentImageY = playgroundY + Math.floor(opponentImageHeight / 2);
    opponentImageScaleX = opponentImageWidth / opponentImage.width;
    opponentImageScaleY = opponentImageHeight / opponentImage.height;

    paddleImage = images.paddle;
    paddleHalfWidth = Math.floor(playgroundHalfHeight / 24);
    paddleWidth = 2 * paddleHalfWidth;
    paddleHeight = paddleWidth * paddleWidthHeightRatio;
    paddleX = playgroundX + playerImageWidth;
    paddleY = playgroundHalfHeight - (paddleHalfWidth * paddleWidthHeightRatio) + playgroundY;
    paddleImageScaleX = paddleWidth / paddleImage.width;
    paddleImageScaleY = paddleHeight / paddleImage.height;

    ballImage = images.ball;
    ballRadius = Math.floor(playgroundHalfHeight / 24);
    ballHeight = 2 * ballRadius;
    ballWidth = ballHeight;
    ballX = playgroundX + playgroundHalfHeight * playgroundHeightWidthRatio;
    ballY = playgroundY + playgroundHalfHeight;
    ballImageScaleX = ballWidth / ballImage.width;
    ballImageScaleY = ballHeight / ballImage.height;

    ballVelocityX = playgroundHeight / 1.5; // The distance to travel per second
    ballVelocityY = playgroundHeight / 1.5; // playgroundHeight / 1.5;
    ballDirectionX = 1; // right = 1, left = -1
    ballDirectionY = -1; // down = 1, up = -1

    Konva.pixelRatio = 1;

    var stage = new Konva.Stage({
        container: 'container',
        width: viewportWidth,
        height: viewportHeight
    });

    var backgroundLayer = new Konva.Layer();
    var foregroundLayer = new Konva.Layer({
        hitGraphEnabled : false
    });

    var playground = new Konva.Rect({
        x: playgroundX,
        y: playgroundY,
        width: playgroundWidth,
        height: playgroundHeight,
        fillPatternImage: images.background,
        fillPatternRepeat: 'repeat',
        stroke: 'blue',
        strokeWidth: 1,
        transformsEnabled: 'position'
    });

    var player = new Konva.Rect({
        x: playerImageX,
        y: playerImageY,
        width: playerImageWidth,
        height: playerImageHeight,
        fillPatternImage: playerImage,
        fillPatternRepeat: 'no-repeat',
        fillPatternScaleX: playerImageScaleX,
        fillPatternScaleY: playerImageScaleY,
        transformsEnabled: 'position',
        listening: false
    });

    var opponent = new Konva.Rect({
        x: opponentImageX,
        y: opponentImageY,
        width: opponentImageWidth,
        height: opponentImageHeight,
        fillPatternImage: opponentImage,
        fillPatternRepeat: 'no-repeat',
        fillPatternScaleX: opponentImageScaleX,
        fillPatternScaleY: opponentImageScaleY,
        transformsEnabled: 'position',
        listening: false
    });

    var paddle = new Konva.Rect({
        x: paddleX,
        y: paddleY,
        width: paddleWidth,
        height: paddleHeight,
        fillPatternImage: paddleImage,
        fillPatternRepeat: 'no-repeat',
        fillPatternScaleX: paddleImageScaleX,
        fillPatternScaleY: paddleImageScaleY,
        transformsEnabled: 'position',
        listening: false
    });

    var ball = new Konva.Rect({
        x: ballX,
        y: ballY,
        width: ballWidth,
        height: ballHeight,
        fillPatternImage: ballImage,
        fillPatternRepeat: 'no-repeat',
        fillPatternScaleX: ballImageScaleX,
        fillPatternScaleY: ballImageScaleY,
        transformsEnabled: 'position',
        listening: false
    });

    var text = new Konva.Text({
        x: playgroundX + playgroundWidth - 100,
        y: 10,
        fontFamily: 'Calibri',
        fontSize: 24,
        text: '',
        fill: 'red',
        transformsEnabled: 'position',
        listening: false
    });
    text.setText('x: -\ny: -');

    playground.on('mousemove', function () {
        pointerMove();
    });

    playground.on('touchmove', function () {
        pointerMove();
    });

    function pointerMove() {
        var position = stage.getPointerPosition();
        var x = Math.floor(position.x) - playgroundX;
        var y = Math.floor(position.y) - playgroundY;

        text.setText('x: ' + x + '\ny: ' + y);

        newPaddlePosition = y + playgroundMargin - paddleHalfWidth * paddleWidthHeightRatio;
        if (newPaddlePosition < playgroundY) {
          newPaddlePosition = playgroundY;
        }
        if((newPaddlePosition + paddleHeight) > (playgroundY + playgroundHeight)) {
          newPaddlePosition = playgroundY + playgroundHeight - paddleHeight
        }
        paddle.y(newPaddlePosition);

        foregroundLayer.batchDraw();
    }

    backgroundLayer.add(playground);
    backgroundLayer.add(player);
    backgroundLayer.add(opponent);
    foregroundLayer.add(paddle);
    foregroundLayer.add(ball);
    foregroundLayer.add(text);
    stage.add(backgroundLayer, foregroundLayer);

    var animation = new Konva.Animation(function(frame) {
        accumulatedTimeDiff += frame.timeDiff;
		if(accumulatedTimeDiff > renderingElapsedTimeThreshold) {
            ballDeltaX = ballVelocityX * (accumulatedTimeDiff / 1000);
			ballDeltaY = ballVelocityY * (accumulatedTimeDiff / 1000);

			ballX = ball.x();
			ballY = ball.y();
			paddleX = paddle.x();
			paddleY = paddle.y();

			if (ballX + ballDeltaX > playgroundX + playgroundWidth - ballRadius) {
				ballDirectionX = -1;
			} else if (
				ballX + ballDeltaX < playgroundX + playerImageWidth + paddleWidth + ballRadius &&
				ballX + ballDeltaX > playgroundX + playerImageWidth + ballRadius &&
				ballY + ballDeltaY < playgroundY + paddleY + paddleHeight - ballRadius &&
				ballY + ballDeltaY > playgroundY + paddleY - ballHeight) {
				ballDirectionX = 1;
			} else if (ballX + ballDeltaX < playgroundX + ballRadius) {
				// This should mean GameOver or 1 life less
				ballDirectionX = 1;
			}

			if (ballY + ballDeltaY > playgroundY + playgroundHeight - ballRadius) {
				ballDirectionY = -1;
			} else if (ballY + ballDeltaY < playgroundY + ballRadius) {
				ballDirectionY = 1;
			}

			ball.x(ballX + ballDirectionX * ballDeltaX);
			ball.y(ballY + ballDirectionY * ballDeltaY);

			// text.setText('' + ballVelocityX * (frame.timeDiff / 1000));
			// text.setText('acc: ' + accumulatedTimeDiff);

			accumulatedTimeDiff = 0;
        } else {
            return false;
        }
    }, foregroundLayer);

    animation.start();
}
