var sources = {
    background: 'assets/background.png',
    dev: 'assets/dev.png',
    ops: 'assets/ops.png',
    paddle: 'assets/paddle.png',
    ball: 'assets/ball.png'
};

loadImages(sources, function(images){
    draw(images);
});
